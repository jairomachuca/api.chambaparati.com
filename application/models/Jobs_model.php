<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobs_model extends CI_Model
{

  public function __construct()
  {
      parent::__construct();
  }

  public function get($id = null)
  {
      if (!is_null($id)) {
          // $query = $this->db->select('*')->from('jobs')->where('id_job', $id)->get();
          $query = $this->db->query("SELECT a.*, b.name name_category, c.name name_modality FROM jobs a
          inner join categories b on a.id_category = b.id_category
          inner join modalities c on a.id_modality = c.id_modality
          where a.id_job =".$id);
          if ($query->num_rows() === 1) {
              return $query->row_array();
          }
          return null;
      }


      $query = $this->db->query("SELECT a.*, b.name name_category, c.name name_modality FROM jobs a
        inner join categories b on a.id_category = b.id_category
        inner join modalities c on a.id_modality = c.id_modality");
      if ($query->num_rows() > 0) {
          return $query->result_array();
      }
      return null;
  }

  public function get_search($query){
    $query = $this->db->query("SELECT a.*, b.name name_category, c.name name_modality FROM jobs a
    inner join categories b on a.id_category = b.id_category
    inner join modalities c on a.id_modality = c.id_modality
    where a.name LIKE '%$query%'");
    if ($query->num_rows() > 0) {
        return $query->result_array();
    }
    return null;
  }

  public function save($job)
    {
        $this->db->set($this->_setJob($job))->insert('jobs');

        if ($this->db->affected_rows() === 1) {
            return $this->db->insert_id();
        }
        return null;
    }

  public function update($id,$job)
     {
         $id = $job['id_job'];
         $this->db->set($this->_setJob($job))->where('id_job', $id)->update('jobs');
         if ($this->db->affected_rows() === 1) {
             return true;
         }
         return null;
     }

  public function delete($id)
    {

        $this->db->where('id_job', $id)->delete('jobs');
        if ($this->db->affected_rows() === 1) {
            return true;
        }
        return null;
    }

  private function _setJob($job)
   {
       return array(
            'id_category' => $job['id_category'],
            'company' => $job['company'],
            'name' => $job['name'],
            'description' => $job['description'],
            'salary_initial' => $job['salary_initial'],
            'salary_last' => $job['salary_last'],
            'published' => $job['published']
       );
   }

}
