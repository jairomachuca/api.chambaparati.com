<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';

class Jobs extends REST_Controller {

  public function __construct(){
      parent::__construct();
      $this->load->model('Jobs_model');
  }

	public function index_get(){    
    $jobs = $this->Jobs_model->get();

    if(!is_null($jobs)){
      foreach($jobs as $job=>$description)
    	{
    	   $descri = $description['description'];
         $descri = substr($descri, 0, 130);
         $jobs[$job]['resumen'] =  $descri;
    	}

      $this->response(array('response'=>$jobs), 200);
    } else {
      $this->response(array('error' => 'No hay registros'), 404);
    }
	}

  public function find_get($id){
    if(!$id){
      $this->response(null, 400);
    }
    $job = $this->Jobs_model->get($id);
    if(!is_null($job)){
      $this->response(array('response'=>$job), 200);
    } else {
      $this->response(array('error' => 'No se encontro registro'), 404);
    }
	}

  public function search_get($query){
    if(!$query){
      $this->response(null, 400);
    }
    $jobs = $this->Jobs_model->get_search($query);
    if(!is_null($jobs)){

      foreach($jobs as $job=>$description)
      {
         $descri = $description['description'];
         $descri = substr($descri, 0, 130);
         $jobs[$job]['resumen'] =  $descri;
      }

      $this->response(array('response'=>$jobs), 200);
    } else {
      $this->response(array('error' => 'No se encontro registro'), 404);
    }
  }

  public function index_post(){
    if(!$this->post('id_category')){
      $this->response(null, 400);
    }
    $id = $this->Jobs_model->save($this->post());
    if(!is_null($id)){
      $this->response(array('response'=>$id), 200);
    }else{
      $this->response(array('error'=>'Algo ha fallado'), 400);
    }
	}

  public function index_put($id)
   {

       if (!$this->put('id_job') || !$id) {
            $this->response(null, 400);
        }

       $update = $this->Jobs_model->update($id,$this->put());
       if (!is_null($update)) {
           $this->response(array('response' => 'registro editado correctamente !'), 200);
       } else {
           $this->response(array('error', 'Algo ha fallado...'), 400);
       }
   }


  public function index_delete($id){
    if(!$id){
      $this->response(null,400);
    }

    $delete = $this->Jobs_model->delete($id);

    if(!is_null($delete)){
      $this->response(array('response'=>'registro eliminado correctamente !'), 200);
    }else{
      $this->response(array('error'=>'Algo ha fallado'), 400);
    }

  }


}
